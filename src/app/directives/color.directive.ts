import { Directive, ElementRef, Input, OnInit } from '@angular/core';
import { RouletteService } from '../shared/roulette.service';

@Directive({
  selector: '[appColor]'
})
export class ColorDirective implements OnInit{
  bgClass!: string;

  @Input() set appColor(bgClass: number) {
    if (bgClass) {
      this.bgClass = this.rouletteService.getColor(bgClass);
    } else if (bgClass === 0) {
      this.bgClass = 'zero';
    }
  };

  constructor(private el: ElementRef, private rouletteService: RouletteService) {}

  ngOnInit() {
    this.el.nativeElement.classList.add(this.bgClass);
  }
}

