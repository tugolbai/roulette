import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { RouletteService } from './shared/roulette.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  numbers: number[] = [];
  total = 100;
  mode = 'black';
  @ViewChild('inputElement') inputElement!: ElementRef;

  constructor(private rouletteService: RouletteService) {}

  ngOnInit(): void {
    this.rouletteService.newNumber.subscribe((number: number) => {
      this.numbers.push(number);
      if (this.rouletteService.getColor(number) === this.mode) {
        this.total += parseInt(this.inputElement.nativeElement.value);
        if (this.rouletteService.getColor(number) === 'zero') {
          this.total += (parseInt(this.inputElement.nativeElement.value) * 35);
        }
      } else {
        this.total -= parseInt(this.inputElement.nativeElement.value);
      }
    });
  }

  onStart() {
    this.rouletteService.start();
  }

  onStop() {
    this.rouletteService.stop();
  }

  onReset() {
    this.numbers = [];
    this.total = 100;
    this.rouletteService.stop();
  }
}
