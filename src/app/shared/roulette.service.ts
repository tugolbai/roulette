import { EventEmitter } from '@angular/core';

export class RouletteService {
  newNumber = new EventEmitter<number>();

  interval!: number;

  generateNumber() {
    return Math.floor(Math.random() * 37);
  }

  start() {
    if (this.interval) return;
    this.interval = setInterval(() => {
      this.newNumber.emit(this.generateNumber());
    }, 1000);
  }

  stop() {
    clearInterval(this.interval);
    this.interval = 0;
  }

  getColor(number: number) {
    if ((number >= 1 && number < 11) || (number >= 19 && number < 29)) {
        if (number % 2) {
          return 'red';
        } else {
          return 'black';
        }
    } else if ((number >= 11 && number < 19) || (number >= 29 && number < 37)) {
        if (number % 2) {
          return 'black';
        } else {
          return 'red';
        }
    } else if (number === 0) {
      return 'zero';
    } else {
      return 'unknown'
    }
  }
}
